package com.google.common.primitives;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class UnsignedShortAdapter extends XmlAdapter<Integer, UnsignedShort> {

	public UnsignedShort unmarshal(Integer v) throws Exception {
		return UnsignedShort.valueOf(v);
	}

	public Integer marshal(UnsignedShort v) throws Exception {
		return v.intValue();
	}

}
