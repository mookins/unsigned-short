package com.google.common.primitives;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class UnsignedIntegerAdapter extends XmlAdapter<Long, UnsignedInteger> {

	public UnsignedInteger unmarshal(Long v) throws Exception {
		return UnsignedInteger.valueOf(v);
	}

	public Long marshal(UnsignedInteger v) throws Exception {
		return v.longValue();
	}
}
